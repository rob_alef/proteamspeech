# README #

Use the main branch as your example for implementing SMS code. 

Use the RoberABranch as your example for implementing voice google search and voice google location code.  

### What is this repository for? ###

You should have a clear idea of how to parse the voice stream returned and a decent idea of what the possibilities are with this stream. 

If you want to implement voice activation and interaction, that example code borrowed from google is in a zip folder on the RobertABranch.  Use that code along with the slides to help you get an idea of how that feature works.

### How do I get set up? ###

You must use an actual phone to test our voice commands. The emulator will not work.  Once plugging in your phone, select it as the test device option, and the application will load onto your phone.