package edu.uchicago.alef.prospeechrecognitionbasic;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "TAG";
    private TextView mTextView;
    private Button mButton;
    //necessary because you could have multiple differnt startActivities for result. Can be any # >= 0
    public static final int VOICE_RECOGNITION_REQUEST_CODE  = 1234;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButton = (Button) findViewById(R.id.speech_button);
        mButton.setOnClickListener(this);
        mTextView = (TextView) findViewById(R.id.txt_output);
    }

    public void startSpeechRecognition(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_WEB_SEARCH_ONLY,
                RecognizerIntent.EXTRA_LANGUAGE_MODEL);
        //extra prompt to help user when they begin speaking
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Speech to text lab 1");
        //SUPER important. without startActivityForResult, onActivityResult won't work.
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    @Override
    public void onClick(View v) {
        startSpeechRecognition();
    }

    public void cameraOption(){
        startActivity(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));
    }

    public void smsOption(String number){

        Uri uri = Uri.parse("smsto:" + number);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", "The SMS text");
        startActivity(it);

    }

    public String getPhoneNumber(String name, Context context) {
        String ret = null;
        String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" like'%" + name +"%'";
        String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor c = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection, selection, null, null);
        if (c.moveToFirst()) {
            ret = c.getString(0);
        }
        c.close();
        if(ret==null)
            ret = "Unsaved";
        return ret;
    }




    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        //make sure request code and the result of the speech2text api worked.  Then do your code stuff
        if(requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK){
            //build the data structure you want to parse from the data.
            ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            StringBuilder spokenWords = new StringBuilder();
            String text = "" + matches.get(0);
            for(Object word : matches){
                spokenWords.append(word + " ");
            }
            //if you said the word camera, run camera code
            mTextView.setText(spokenWords);
            if(matches.contains("camera")){
                cameraOption();
            }

            else if(text.contains("send a message")){
                String contactName = text.substring(text.indexOf("to ")+3);
                String number = getPhoneNumber(contactName, MainActivity.this);
                smsOption(number);
            }

            //just take the first result that you have and search for it.
            //note: with the final project, you can check for the word 'in' and use that as your splitter to search place 'in' city
            else{
                Uri uri = Uri.parse("http://www.google.com/#q="+matches.get(0));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        }
    }


}